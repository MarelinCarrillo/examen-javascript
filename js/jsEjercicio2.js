
document.addEventListener("DOMContentLoaded", function() {
    var cantidadInput = document.getElementById("cantidad");
    var resultado = document.getElementById("resultado");
    var celsiusToFahrenheitRadio = document.getElementById("celsiusToFahrenheit");
    var fahrenheitToCelsiusRadio = document.getElementById("fahrenheitToCelsius");
    var calcularButton = document.getElementById("calcular");
    var limpiarButton = document.getElementById("limpiar");

    function convertirCelsiusToFahrenheit(celsius) {
        return (9/5 * celsius) + 32;
    }

    function convertirFahrenheitToCelsius(fahrenheit) {
        return (5/9) * (fahrenheit - 32);
    }

    calcularButton.addEventListener("click", function() {
        var cantidad = parseFloat(cantidadInput.value);
        var conversion;

        if (celsiusToFahrenheitRadio.checked) {
            conversion = convertirCelsiusToFahrenheit(cantidad);
            resultado.textContent = cantidad + " °Celsius es igual a " + conversion.toFixed(2) + " °Fahrenheit.";
        } else if (fahrenheitToCelsiusRadio.checked) {
            conversion = convertirFahrenheitToCelsius(cantidad);
            resultado.textContent = cantidad + " °Fahrenheit es igual a " + conversion.toFixed(2) + " °Celsius.";
        }
    });

    limpiarButton.addEventListener("click", function() {
        cantidadInput.value = "";
        resultado.textContent = "";
    });
});
