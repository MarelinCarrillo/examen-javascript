let edades = [];

function generarEdades() {
    for (let i = 0; i < 100; i++) {
        edades.push(Math.floor(Math.random() * 91)); // Genera edades aleatorias de 0 a 90 años.
    }
    mostrarEdades();
    calcularTotales();
    calcularPromedio();
}

function mostrarEdades() {
    document.getElementById("edades").innerText = edades.join(", ");
}

function calcularTotales() {
    const bebeCount = edades.filter(edad => edad >= 1 && edad <= 3).length;
    const ninoCount = edades.filter(edad => edad >= 4 && edad <= 12).length;
    const adolescenteCount = edades.filter(edad => edad >= 13 && edad <= 17).length;
    const adultoCount = edades.filter(edad => edad >= 18 && edad <= 60).length;
    const ancianoCount = edades.filter(edad => edad >= 61 && edad <= 90).length;

    document.getElementById("bebeCount").innerText = bebeCount;
    document.getElementById("ninoCount").innerText = ninoCount;
    document.getElementById("adolescenteCount").innerText = adolescenteCount;
    document.getElementById("adultoCount").innerText = adultoCount;
    document.getElementById("ancianoCount").innerText = ancianoCount;
}

function calcularPromedio() {
    const totalEdades = edades.reduce((total, edad) => total + edad, 0);
    const promedio = (totalEdades / edades.length).toFixed(2);
    document.getElementById("promedio").innerText = promedio;
}

function limpiarEdades() {
    edades = [];
    document.getElementById("edades").innerText = "";
    document.getElementById("bebeCount").innerText = "0";
    document.getElementById("ninoCount").innerText = "0";
    document.getElementById("adolescenteCount").innerText = "0";
    document.getElementById("adultoCount").innerText = "0";
    document.getElementById("ancianoCount").innerText = "0";
    document.getElementById("promedio").innerText = "0";
}

